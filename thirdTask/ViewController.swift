//
//  ViewController.swift
//  thirdTask
//
//  Created by uros.rupar on 5/12/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        username.layer.cornerRadius = 15
        password.layer.cornerRadius = 15
        button.layer.cornerRadius = 15

    }

    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var button: UIButton!
}

